<?php
    session_start();
    include('connex.inc.php');
    include('liste_mangas.inc.php');

    /*l'utilisateur veut s'identifier*/
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
    /*l'administrateur ajoute un titre*/
    else{
        if($_SESSION['statut']==1){
            ajouter_manga('isekai');
        }
    }
?>

<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
  </head>

  <body>

	  <?php
	  include("header.inc.php");

	  /*------------Affiche le formulaire d'ajout de mangas seulement pour les administrateurs-----------*/
	   <?php
	   if(isset($_SESSION['pseudo']) && isset($_SESSION['statut']) && ($_SESSION['statut']==1)){
		   if(isset($erreur)){
			afficher_formulaire_ajout($erreur);
		}
		else{
			afficher_formulaire_ajout(null);
		}
		}
	  ?>

<footer>
  <p>
  Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
</footer>




</body>
</html>
