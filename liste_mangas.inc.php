<?php


/*Parcours de la table mangas et affiche tout les mangas présents dans la catégorie correspondant à la page dans laquelle on se trouve
Cette fonction est utilisée dans les pages des thèmes.*/
function afficher_liste($genre){
    $pdo= connex();
    try{
        $sql=$pdo->prepare("SELECT * FROM manga WHERE genre=:genre");
        $sql->bindParam(":genre",$genre);
        $sql->execute();

    }
    catch(PDOException $e){
        echo $e->getMessage();
    }

    /*Parcours du tableau renvoyé et affichage*/
    echo "<div class=\"block_image\">";

    foreach($sql as $manga){
        echo "<div class=contenu>  <a href=\"".$genre.".php?id=".$manga['ID']."\"><img src=\"".$manga['couverture']."\" alt=\"couverture\" class=\"image_liste\" /></a>  <p class=\"titre_manga\">".$manga['titre']."</p> ";
        if(isset($_SESSION['pseudo']) && isset($_SESSION['statut'])){
            echo "<input type=\"submit\" onclick=\"window.location.href='ajouter_collection.php?id=".$manga['ID']."'\" name=\"ajouter_collection\" value=\"Ajouter à ma collection\" class=\"button1\"/>";
            if($_SESSION['statut']==1){
                echo "<br><input type=\"submit\" onclick=\"window.location.href='supprimer.php?id=".$manga['ID']."'\" name=\"supprimer\" value=\"Supprimer\" class=\"button1\"/>";
            }
        }
        echo "</div>";
    }
}


/*Affichage du formulaire d'ajout de mangas dans la base de données visible par l'administrateur.
Cette fonction est utilisée dans les pages des thèmes.*/
function afficher_formulaire_ajout($erreur){

     echo "<div class=\"ajoute\">
     <div class=\"head\">
           <h3 class=\"entete\">Partie Administrateur</h3>
         </div>

       <form name=\"ajoute_livre\" action=".htmlspecialchars($_SERVER['PHP_SELF'])." method=\"post\" enctype=\"multipart/form-data\">

         <fieldset>
         <legend><strong>Ajouter un livre</strong></legend>

         <label>Auteur<input type=\"text\" name=\"auteur\" id=\"auteur\" required /></label><br>
           <label>Titre<input type=\"text\" name=\"titre\" id=\"titre_manga\" required /></label><br>
           <label>Ann&eacute;e de parution<input type=\"text\" name=\"annee\" id=\"annee_parution\" maxlength=\"4\" required /></label><br>
             <label>Description<br><textarea name=\"description\" id=\"description\"  required></textarea></label><br>

           <label>Couverture<input type=\"file\" id=\"couverture\" name=\"couverture\" accept=\".png,.jpeg,.jpg\"/></label>
           <br>";
           if(isset($erreur)){
               echo "<p>$erreur</p>";
           }
         echo "<input type=\"submit\" value=\"Ajouter\" name=\"ajouter\" class=\"button1\"/>
           </fieldset>
           </form>


         </div>";

}

/*Affichage du formulaire de modification de mangas dans la base de données visible par l'administrateur.
Cette fonction est utilisée dans la pages modifier_infos*/
function afficher_formulaire_modif($id, $erreur){
    try{
        $pseudo=$_SESSION['pseudo'];
        $pdo= connex();
        $sql=$pdo->prepare("SELECT * FROM manga,collection WHERE manga.ID=$id");
        /*$sql->bindParam(":pseudo",$pseudo);*/
        $sql->execute();
    }
    catch(PDOException $e){
        echo $e->getMessage();
    }

     echo "<div class=\"modif\">
     <div class=\"head\">
           <h3 class=\"entete\">Modifier un livre</h3>
         </div>

       <form name=\"modif_livre\" action=".htmlspecialchars($_SERVER['PHP_SELF'])." method=\"post\" enctype=\"multipart/form-data\">

         <fieldset>
         <legend><strong>Modifier un livre</strong></legend>

         <label>Auteur<input type=\"text\" name=\"auteur\" id=\"auteur\" value=\"".$sql['auteur']."\" /></label><br>
           <label>Titre<input type=\"text\" name=\"titre\" id=\"titre_manga\" value=\"".$sql['titre']."\"  /></label><br>
           <label>Année de parution<input type=\"text\" name=\"annee\" id=\"annee_parution\" maxlength=\"4\" value=\"".$sql['annee']."\" /></label><br>
             <label>Description<br><textarea name=\"description\" id=\"description\" value=\"".$sql['description']."\" ></textarea></label><br>

           <label>Couverture<input type=\"file\" id=\"couverture\" name=\"couverture\" accept=\".png,.jpeg,.jpg\"/></label>
           <br>";
           if(isset($erreur)){
               echo "<p>$erreur</p>";
           }
         echo "<input type=\"submit\" value=\"Modifier\" name=\"modifier\" class=\"button1\"/>
           </fieldset>
           </form>


         </div>";

}

/*Ajoute un manga dans la base de données
Cette fonction est utilisée dans les pages des thèmes lorsqu'un administrateur envoie le formulaire d'ajout*/
function ajouter_manga($genre){

    if( isset($_POST['ajouter']) ){

        /*Si l'image a bien été uploadé, on vérifie son extension et sa taille*/
        if( isset($_FILES['couverture']) ){
            if($_FILES['couverture']['error'] != 0 || $_FILES['couverture']['size']>2000000){
            $erreur="Erreur lors du chargement de l'image.";
            }
            else{
            $extensions = array('png','jpg', 'jpeg');
            $nom_fichier = pathinfo($_FILES['couverture']['name']);
            $extension_upload = $nom_fichier['extension'];
            if (!in_array($extension_upload, $extensions)){
                $erreur="Le fichier n'a pas la bonne extension.";
            }

            /*On vérifie que le titre n'est pas déjà présent dans la base puis on l'ajoute*/
        else if( isset($_POST['titre']) && isset($_POST['auteur']) && isset($_POST['annee']) && isset($_POST['description']) ){
            try{
            $pdo= connex();
            $titre=trim($_POST['titre']);
            $auteur=trim($_POST['auteur']);
            $nom_fichier="couverture_".$titre.".".$extension_upload;
            $resultat = move_uploaded_file($_FILES['couverture']['tmp_name'],"image/".$nom_fichier);


            $fichier="image/".$nom_fichier;
            $sql = $pdo->prepare("SELECT * FROM manga WHERE titre=:titre AND auteur=:auteur");
            $sql->bindParam(":titre",$titre);
            $sql->bindParam(":auteur",$auteur);
            $sql->execute();

            $n=$sql->fetchAll(PDO::FETCH_ASSOC);

            if(count($n)==0){
                $sql = $pdo->prepare("INSERT INTO manga (titre,auteur,annee_parution,genre,description,couverture) VALUES(:titre,:auteur,:annee, :genre, :description, :couverture)");
                $sql->bindParam(":titre",$titre);
                $sql->bindParam(":auteur",$auteur);
                $sql->bindParam(":annee",$_POST['annee']);
                $sql->bindParam(":genre",$genre);
                $sql->bindParam(":description",$_POST['description']);
                $sql->bindParam(":couverture",$fichier);
                $sql->execute();
                echo "<script type=\"text/javascript\">alert(\"Le titre a bien été ajouté.\");</script> ";
            }
            else{
                echo "<script type=\"text/javascript\">alert(\"Ce titre est déjà enregistré.\");</script> ";
            }
            $pdo=null;
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
        }
        }
        }
    }
    /*Cette erreur est renvoyée si le fichier uploadé n'est pas valide et le message est affiché dans le formulaire*/
    if(isset($erreur)) return $erreur;

}

?>
