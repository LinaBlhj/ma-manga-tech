<?php
	session_start();
	include 'connex.inc.php';
	if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
		identification();
	}
?>

<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
  </head>

  <body>

	  <?php
	  include("header.inc.php");

	  /*Affichage du formulaire d'inscription si l'utilisateur n'est pas connecté*/
		if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
    echo "<div id=\"cadre1\">
      <p>Pas encore inscrit?<br><br>Inscrivez-vous et r&eacute;alisez votre manga-tech avec vos mangas préférés!<br></p>

      <form id=\"inscription\"action=\"inscription.php\" method=\"post\" name=\"inscription\">
	<fieldset>
	  <legend><strong>Inscription Gratuite</strong></legend>
	  <label><input type=\"text\" name=\"pseudo\" placeholder=\"Choisir un pseudo\" required=\"required\"/></label><br>
	  <label><input type=\"email\" name=\"mail\" placeholder=\"Adresse mail\" required=\"required\"/></label><br>
	  <input type=\"submit\" value=\"S'inscrire\" name=\"inscription\" class=\"button1\"/>
	</fieldset>
      </form>
    </div>";
		}
?>


<!--------Affichage des catégories---------->
    <div id="cadre2">

      <div class="head">
      <h3 class="entete">Thèmes</h3>
      </div>


	<div class="block_image">

      <div class="contenu">
	<a href="shojo.php" class="theme"><img class="image"
			       src="image/shojo.jpeg"
			       alt="shojo"
			       height="200"/>
	  <div class="nom">SHOJO</div>
	  </a>
      </div>

      <div class="contenu">
	<a href="shonen.php" class="theme"><img class="image"
				 src="image/shonen.jpg"
				 alt="Shonen"
				 height="200"/>
	  <div class="nom">SHONEN</div>
	</a>
      </div>


      <div class="contenu">
	<a href="isekai.php" class="theme"><img class="image"
				 src="image/isekai.jpeg"
				 alt="isekai"
				 height="200"/>
	<div class="nom">ISEKAI</div>
	</a>
      </div>

      <div class="contenu">
	<a href="seinen.php" class="theme" ><img class="image"
				 src="image/seinen.jpeg"
				 alt="Seinen"
				 height="200"/>
          <div class="nom">SEINEN</div>
	  </a>
      </div>


      <div class="contenu">
	<a href="coreen.php" class="theme" ><img class="image"
				  src="image/coreens.jpeg"
				  alt="coreens"
				  height="200"/>
	  <div class="nom">COREENS</div>
	  </a>
      </div>


      <div class="contenu">
	<a href="historique.php" class="theme" ><img class="image"
				  src="image/historique.jpeg"
				  alt="coreens"
				  height="200"/>
	  <div class="nom">HISTOIRE</div>
	  </a>
      </div>

	</div>

    </div>

    <footer>
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>




  </body>
</html>
