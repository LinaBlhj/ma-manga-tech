<?php
    session_start();
    include 'connex.inc.php';
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
?>


<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
  </head>

  <body>

      <?php
      include("header.inc.php");


      ?>


      <div class="resultats_recherche">
         <div class="head">
           <h3 class="entete">Résultats</h3>
       </div>
       <?php
       $pdo= connex();

       /*Récupération de la recherche de l'utilisateur puis affichage des réultats correspondants */
       try{

           $recherche="%".trim($_GET['recherche'])."%";
           $sql = $pdo->prepare("SELECT * FROM manga WHERE titre LIKE :recherche OR auteur LIKE :recherche");
           $sql->bindParam(":recherche",$recherche);
           $sql->execute();
           $n=$sql->fetchAll(PDO::FETCH_ASSOC);

           if(count($n)==0){
               echo "<p>Aucun titre ou auteur ne correspond à votre recherche.</p>";
           }
           else{
               foreach($n as $resultat){
                   echo "<div class=contenu> <img src=\"".$resultat['couverture']."\" alt=\"couverture\" class=\"image_liste\" />
                   <p class =\"description_resultats\">
                   <strong>Titre:</strong> ".$resultat['titre']."<br><strong>Auteur: </strong>".$resultat['auteur']."<br>
                   <strong>Année de parution: </strong>".$resultat['annee_parution']."<br>".$resultat['description']."</p>
                   <br>";if(isset($_SESSION['pseudo']) && isset($_SESSION['statut'])){
                       echo "<input type=\"submit\" onclick=\"window.location.href='ajouter_collection.php?id=".$resultat['ID']."'\" name=\"ajouter_collection\" value=\"Ajouter à ma collection\" class=\"button1\"/>";
                   }echo "</div><br>";
                   }
           }
       }
       catch(PDOException $e){
           echo $e->getMessage();

       }


       ?>

<!-----------Affichage des catégories---------------->
   </div>
      <div id="cadre2">

        <div class="head">
        <h3 class="entete">Thèmes</h3>
        </div>


  	<div class="block_image">

        <div class="contenu">
  	<a href="shojo.php" class="theme"><img class="image"
  			       src="image/shojo.jpeg"
  			       alt="shojo"
  			       height="200"/>
  	  <div class="nom">SHOJO</div>
  	  </a>
        </div>

        <div class="contenu">
  	<a href="shonen.php" class="theme"><img class="image"
  				 src="image/shonen.jpg"
  				 alt="Shonen"
  				 height="200"/>
  	  <div class="nom">SHONEN</div>
  	</a>
        </div>


        <div class="contenu">
  	<a href="isekai.php" class="theme"><img class="image"
  				 src="image/isekai.jpeg"
  				 alt="isekai"
  				 height="200"/>
  	<div class="nom">ISEKAI</div>
  	</a>
        </div>

        <div class="contenu">
  	<a href="seinen.php" class="theme" ><img class="image"
  				 src="image/seinen.jpeg"
  				 alt="Seinen"
  				 height="200"/>
            <div class="nom">SEINEN</div>
  	  </a>
        </div>


        <div class="contenu">
  	<a href="coreen.php" class="theme" ><img class="image"
  				  src="image/coreens.jpeg"
  				  alt="coreens"
  				  height="200"/>
  	  <div class="nom">COREENS</div>
  	  </a>
        </div>


        <div class="contenu">
  	<a href="historique.php" class="theme" ><img class="image"
  				  src="image/historique.jpeg"
  				  alt="coreens"
  				  height="200"/>
  	  <div class="nom">HISTOIRE</div>
  	  </a>
        </div>

  	</div>

      </div>

      <footer>
        <p>
        Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
      </footer>




    </body>
  </html>
