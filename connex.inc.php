<?php

/*Fonction de connexion à la base de données*/
function connex(){
  try {
      $pdo = new PDO('mysql:host=localhost;dbname=mangatech', "root", "");
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  catch(PDOException $e) {
      echo 'Problème à la connexion';
      echo $e;
      die();
  }

  return $pdo;
}

/*Fonction de connexion d'un utilisateur
Vérifie que le mot de passe et la pseudo sont présents dans la base de données puis ouvre une session*/
function identification(){
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        if(isset($_POST['identification']) && isset($_POST['pseudo']) && isset($_POST['mdp'])){

            $pdo= connex();

            try{

                $pseudo=trim($_POST['pseudo']);
                $mdp=md5(trim($_POST['mdp']));
                $sql = $pdo->prepare("SELECT * FROM membres WHERE pseudo=:pseudo AND mdp=:mdp");
                $sql->bindParam(":pseudo",$pseudo);
                $sql->bindParam(":mdp",$mdp);
                $sql->execute();
                $n=$sql->fetchAll(PDO::FETCH_ASSOC);

                if(count($n)==1){
                    $_SESSION['pseudo']=$pseudo;
                    $_SESSION['statut'] = intval($n[0]['statut']);
                    header('Location:Acceuil.php');
                }
                else{
                    echo "<script type=\"text/javascript\">window.alert(\"Pseudo ou mot de passe incorrect.\");</script>";
                }
            }
            catch(PDOException $e){
                echo $e->getMessage();

            }
        }
    }


}
?>
