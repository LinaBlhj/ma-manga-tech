<?php
    session_start();
    include('connex.inc.php');
    include('liste_mangas.inc.php');

    /*l'utilisateur veut s'identifier*/
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
    /*l'administrateur ajoute un titre*/
    else{
        if($_SESSION['statut']==1){
            $erreur=ajouter_manga('seinen');
        }
    }
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
    <script src="espace_perso.js"></script>
  </head>

  <body class="body_2">


<?php
    include("header.inc.php");
?>

<!----------Bloc description de la catégorie------------->
 <div class="description">
   <div class="head">
       <h3 class="entete">Description</h3>
       </div>
   <p>Le seinen manga est déstiné à un public de jeune adulte. Les scènes y sont plus violentes et réalistes.</p>


 </div>


 <!----------Div caché, s'affiche lorsque l'on clique sur une couverture------------->
 <div class="collection" >
        <?php if(isset($_GET['id'])){
            try{
            $id=$_GET['id'];
            $pdo= connex();
            $sql=$pdo->prepare("SELECT * FROM manga WHERE ID=:id");
            $sql->bindParam(":id",$id);
            $sql->execute();
            $resultat=$sql->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }

        
        echo "<div class=contenu id=\"contenu\" > <img src=\"".$resultat['couverture']."\" alt=\"couverture\" class=\"image_liste\" /><p class =\"description_resultats\">
        <strong>Titre:</strong> ".$resultat['titre']."<br><strong>Auteur: </strong>".$resultat['auteur']."<br><strong>Année de parution: </strong>".$resultat['annee_parution']."<br><strong>Genre: </strong>".$resultat['genre']."<br>".$resultat['description']."</p>
        <br></div>";
        echo "<script>afficher_titre();</script>";
    }?>
    </div>


    <!---------Affiche les mangas présents dans cette catégorie----------->
 <div class="liste_livre">
    <div class="head">
      <h3 class="entete">Liste</h3>
    </div>
    <?php
        afficher_liste('seinen');
    echo "</div>";
    ?>

   </div>


  <!------------Affiche le formulaire d'ajout de mangas seulement pour les administrateurs----------->
   <?php
 		if(isset($_SESSION['pseudo']) && isset($_SESSION['statut']) && ($_SESSION['statut']==1)){
            if(isset($erreur)){
             afficher_formulaire_ajout($erreur);
            }
            else{
             afficher_formulaire_ajout(null);
            }
     }
?>


 <footer class="footer3">
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>




  </body>
</html>
