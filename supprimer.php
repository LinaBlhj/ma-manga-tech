<?php
session_start();
include 'connex.inc.php';

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Supprimer</title>
    </head>

    <body>

    <?php

        if( (isset($_SESSION["pseudo"])) && (isset($_SESSION["statut"])) ){
            /*Suppression d'un manga de la base de données*/
            if(isset($_GET["id"]) ){
                if($_SESSION["statut"]==1){


                    $pdo = connex();
                    try{
                    $id=$_GET["id"];
                    $requete=$pdo->prepare("SELECT genre,couverture FROM manga WHERE ID=:id");
                    $requete->bindParam(":id",$id);
                    $requete->execute();
                    $manga=$requete->fetch();
                    $requete=$pdo->prepare("DELETE FROM manga WHERE ID=:id");
                    $requete->bindParam(":id",$id);

                    $requete->execute();
                    $nb=$requete->rowCount();
                    if(count($nb)==1){
                        unlink($manga['couverture']);
                        $genre=$manga['genre'];
                        header("Location:$genre.php");
                        }
                        else{
                        header("Location:$genre.php");
                        }

                        $pdo=null;

                }
                catch(PDOException $e){
                    echo '<p>Problème à l\'exécution</p>';
                    echo $e->getMessage();
                    die();
                }
                }
                else{
                    echo "Vous ne pouvez pas accéder à cette page.";
                }
            }
            /*Suppression d'un membre de la base de données*/
                else if(isset($_GET['id_membre'])){
                    if($_SESSION["statut"]==1){
                    $pdo = connex();
                    try{
                    $id=$_GET["id_membre"];

                    $requete=$pdo->prepare("DELETE FROM membres WHERE pseudo=:id");
                    $requete->bindParam(":id",$id);

                    $requete->execute();
                    $nb=$requete->rowCount();
                    if($nb==1){
                        unlink($manga['couverture']);
                        $genre=$manga['genre'];
                        header("Location:gestion_membre.php");
                        }
                        else{

                        }
                        $requete->closeCursor();
                        $pdo=null;

                }
                catch(PDOException $e){
                    echo '<p>Problème à l\'exécution</p>';
                    echo $e->getMessage();
                    die();
                }
            }
            else{
                echo "Vous ne pouvez pas accéder à cette page.";
            }
        }
            /*Suppression d'une titre de la collection d'un membre*/
            if(isset($_GET['id_collection'])){
                $pdo = connex();
                try{
                $id=$_GET["id_collection"];
                $pseudo=$_SESSION['pseudo'];
                $requete=$pdo->prepare("DELETE FROM collection WHERE ID_manga=:id AND pseudo=:pseudo");
                $requete->bindParam(":id",$id);
                $requete->bindParam(":pseudo",$pseudo);
                $requete->execute();
                $nb=$requete->rowCount();
                if($nb==1){
                    $genre=$manga['genre'];
                    header("Location:Espace_perso.php");
                    }
                    else{
                        echo "Ce titre n'est pas présent dans votre collection.";
                    }
                    $requete->closeCursor();
                    $pdo=null;

                }
                catch(PDOException $e){
                echo '<p>Problème à l\'exécution</p>';
                echo $e->getMessage();
                die();
                }
                }
                else{
                echo "Ce titre n'existe pas.";
                }


        }


    ?>

    </body>

</html>
