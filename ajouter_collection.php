<?php
session_start();


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Supprimer</title>
    </head>

    <body>

    <?php

    /*Ajout d'une entrée dans la table collection
    On vérifie que l'utilisateur est connecté, et que le titre n'est pas déjà présent dans sa collection.*/
        if( (isset($_SESSION["pseudo"])) && (isset($_SESSION["statut"])) ){

                if(isset($_GET["id"])){

                    include 'connex.inc.php';
                    $pdo = connex();
                    try{
                    $id=$_GET["id"];
                    $pseudo=$_SESSION['pseudo'];

                    $requete=$pdo->prepare("SELECT ID_manga FROM collection WHERE ID_manga=:id AND pseudo=:pseudo");
                    $requete->bindParam(":pseudo",$pseudo);
                    $requete->bindParam(":id",$id);
                    $requete->execute();
                    $n=$requete->fetchAll(PDO::FETCH_ASSOC);
                    if(count($n)!=0){

                        header("Location:Espace_perso.php?erreur=doublon");
                        }
                    else{
                    $requete=$pdo->prepare("INSERT INTO collection VALUES(:pseudo,:id)");
                    $requete->bindParam(":id",$id);
                    $requete->bindParam(":pseudo",$pseudo);
                    $requete->execute();
                    $nb=$requete->rowCount();
                    if($nb==1){
                        header("Location:Espace_perso.php");
                        }
                        else{

                        }
                        $pdo=null;
                    }
                }
                catch(PDOException $e){
                    echo $e->getMessage();
                }
                }
                else{
                    echo "Ce titre n'existe pas.";
            }
        }
            else{
                header("Location:Espace_perso.php?erreur=acces");

            }



    ?>

    </body>
