<?php
    session_start();
    include 'connex.inc.php';
    if(!isset($_SESSION['pseudo']) && !isset($_SESSION['statut'])){
        identification();
    }
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Ma manga-tech</title>
    <link rel="stylesheet" href="acceuil.css">
  </head>

  <body>

    <?php
    include("header.inc.php");

    if(isset($_SESSION['pseudo']) && isset($_SESSION['statut']) && $_SESSION['statut']==1 ){
    echo "<div class=\"liste_membre\">


    <div class=\"head\">
      <h3 class=\"entete\">Membres</h3>
    </div>";


/*Affichage d'un tableau avec le pseudo de chaque membre et un bouton pour le supprimer de la base de données.
Le bouton renvoie sur la page supprimer.php*/
    $pdo= connex();
    try{
        $sql=$pdo->query("SELECT * FROM membres WHERE statut='0'");
        echo "<table><tr><th>Pseudo</th>";
        echo "<th>Gérer</th><tr>";

        foreach($sql as $cle){
            echo "<tr><td class=\"td1\">",$cle['pseudo'],"</td>";
            echo "<td><input type=\"submit\" onclick=\"window.location.href='supprimer.php?id_membre=".$cle['pseudo']."'\" name=\"supprimer\" value=\"Supprimer\" class=\"button1\"/></td>";

            }
            echo "</tr>";

        echo "</table>";
        $sql->closeCursor();
        $pdo=null;

    }
    catch(PDOException $e){
        echo $e->getMessage();
    }

    echo "</div>";
    }
    /*Si l'utilisateur n'est pas administrateur il n'a pas accès à cette page.*/
		else{
			echo "
      <div class=\"mauvais_acces\">

     <p>Vous n'avez pas accès à cette page.</p>

  </div>
      ";
		}
		?>







    <footer>
      <p>
      Contact<br> lina.belhadj@etu.univ-st-etienne.fr/maryam.eid@etu.univ-st-etienne.fr</p>
    </footer>
  </body>
</html>
